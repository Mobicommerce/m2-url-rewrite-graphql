<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_UrlRewriteGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\UrlRewriteGraphQl\Plugin\Magento\UrlRewriteGraphQl\Model\Resolver;

class EntityUrl
{
    public function __construct(
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Mobicommerce\Mobiapp\Helper\CmsPage $cmsPageHelper
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->pageFactory = $pageFactory;
        $this->cmsPageHelper = $cmsPageHelper;
    }

    public function afterResolve($subject, $result)
    {
       
        if (isset($result['type']) && ($result['type'] == 'CMS_PAGE' || $result['type'] == 'CATEGORY')) {
            if ($result['type'] == 'CATEGORY') {
                $model = $this->categoryRepository->get($result['id']);

                $result['mobicommerce_landing_page_enable'] = $model->getData('mobicommerce_landing_page_enable');
                $result['mobicommerce_seo_widget_ids'] = $model->getData('mobicommerce_seo_widget_ids');
            } elseif ($result['type'] == 'CMS_PAGE') {
                $model = $this->pageFactory->create()->load($result['id']);

                $result['mobicommerce_page_type'] = $model->getData('mobicommerce_page_type');
                $result['mobicommerce_product_collection_id'] = $model->getData('mobicommerce_product_collection_id');
                $result['mobicommerce_seo_widget_ids'] = $this->cmsPageHelper->getMobicommerceSeoWidgets(
                    $model->getId()
                );
            }
            
            $result['mobicommerce_pwa_landing_page_id'] = $model->getData('mobicommerce_pwa_landing_page_id');
            $result['mobicommerce_desktop_landing_page_id'] = $model->getData('mobicommerce_desktop_landing_page_id');
            $result['mobicommerce_mobileapp_landing_page_id'] = $model->getData(
                'mobicommerce_mobileapp_landing_page_id'
            );
        }

        return $result;
    }
}
